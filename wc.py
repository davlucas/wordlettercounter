#!/usr/bin/env python
import sys

def app(lettres):
    for i in lettres:
        i=i.split()
        words.extend(i.split(']'))
    for word in words:
        if word.upper() in dictwords and word.isalpha():
            dictwords[str(word.upper())]+=1
        else:
            dictwords[str(word.upper())]=1
    tableword=[[values,keys] for keys,values in dictwords.items()]
    tableword.sort()
    for element in tableword:
        print("{} appears {} times".format (element[1],element[0]))
words=[]
dictwords=dict(
)
try:
    with open(sys.argv[1]) as f:
        lettres= f.readlines()
        app(lettres)
except IndexError:
    print('usage: $lc.py file.txt')
except FileNotFoundError:
    print('File {} does not exist'.format(sys.argv[1]))
except PermissionError:
    print("You don't have privilege to read {}".format(sys.argv[1]))
