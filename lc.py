#!/usr/bin/env python

import sys

def app(lettres):
    for line in lettres:
        for i in line:
            if i.upper() in lettercount and i.isalpha():
                lettercount[str(i.upper())]+=1
            else:
                lettercount[str(i.upper())]=1
    for (key,value) in lettercount.items():
        listlettres.append([int(value),str(key)])
    listlettres.sort(reverse=True)
    if listlettres:
        for i in listlettres:
            print("{} appears {} times".format (i[1],i[0]))
    else:
        print('fichier {} vide'.format(sys.argv[1]))


lettres=[]
listlettres=[]
lettercount=dict()
try:
    with open(sys.argv[1]) as f:
        lettres= [list(line.rstrip()) for line in f]
        app(lettres)
except IndexError:
    print('usage: $lc.py file.txt')
except FileNotFoundError:
    print('File {} does not exist'.format(sys.argv[1]))
except PermissionError:
    print("You don't have privilege to read {}".format(sys.argv[1]))
