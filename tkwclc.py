#!/usr/bin/env python
from tkinter import (
    Tk,
    Button,
    Entry,
    StringVar,
    LabelFrame,
    Listbox,
    CENTER
)
from tkinter.messagebox import showerror
from tkinter import filedialog
import sys


class App:
    def __init__(self):
        root = Tk()
        self.txtfile = StringVar()
        self.file = "0"
        root.title("Word and letter counter")
        frameup = LabelFrame(root, text="file info")
        Button(frameup, text="Choose a textfile", command=self.browse).grid(
            column=0, row=0
        )
        Entry(frameup, textvariable=self.txtfile).grid(column=1, row=0)
        Button(
            frameup, text="Frequency of words", command=self.wordcount
        ).grid(column=0, row=1)
        Button(
            frameup, text="Frequency of letters", command=self.lettercount
        ).grid(column=1, row=1)
        frameup.pack()
        self.framedown = LabelFrame(root)
        self.listbox = Listbox(self.framedown,relief='groove',borderwidth=3,font=("Helvetica",14),height=20,width=30,justify=CENTER)
        self.framedown.pack()
        self.listbox.pack()
        root.mainloop()

    def browse(self):
        file = filedialog.askopenfilename()
        self.txtfile.set(file)

    def lettercount(self):
        self.check_file()
        lettres = [list(line.rstrip()) for line in self.file]
        self.file.close()
        self.framedown.config(text='Letter count')
        lettercount = dict()
        self.listbox.delete(0, "end")
        for line in lettres:
            for i in line:
                if i.upper() in lettercount and i.isalpha():
                    lettercount[str(i.upper())] += 1
                if i.upper() not in lettercount and i.isalpha():
                    lettercount[str(i.upper())] = 1
        listlettres = [[value, key] for key, value in lettercount.items()]
        somme=sum([value for key, value in lettercount.items()])
        listlettres.sort(reverse=True)
        j = 1
        for i in listlettres:
            print(i)
            self.listbox.insert(
                "end", "{}- {} : {} times -> {:.2f}%".format(j, i[1].upper(), i[0], float(i[0]/somme)*100)
            )
            j += 1

    def wordcount(self):
        self.check_file()
        file = self.file.readlines()
        self.file.close()
        self.framedown.config(text='Word count')
        table2 = []
        dico = dict()
        self.listbox.delete(0, "end")
        for i in file:
            table = i.split()
            for line in table:
                table2.extend(line.split(":"))
        for element in table2:
            element.strip("\n")
            element.strip(',')
        for words in table2:
            if words.lower() in dico and words.isalpha():
                dico[words.lower()] += 1
            if words.lower() not in dico and words.isalpha():
                dico[words.lower()] = 1
        tablefinal = [[values, key] for key, values in dico.items()]
        tablefinal.sort(reverse=True)
        for i in tablefinal:
            self.listbox.insert(
                "end", "{} appears  {} times".format(i[1].upper(), i[0])
            )

    def check_file(self):
        file = self.txtfile.get()
        try:
            self.file = open(file, "r")
        except IndexError:
            showerror("ERROR", "no file selected")
        except FileNotFoundError:
            showerror("ERROR", "no file selected")
        except PermissionError:
            showerror("no file selected")


def main(_args):
    """
    main
    """
    App()
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
